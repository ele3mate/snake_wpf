# Snake_WPF

Hi!

In this repo, you will find my first step into the .NET environment using Visual Studio 2022 and WPF.
It is a remake of an old-school retro Snake game.
To make it look better, I got some help via a YT tutorial but I have added some juice. 
Use your keyboard arrows to move and hopefully you will enjoy it!

More projects to come.

